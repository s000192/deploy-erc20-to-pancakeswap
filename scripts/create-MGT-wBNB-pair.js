// We require the Hardhat Runtime Environment explicitly here. This is optional 
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");

async function main() {
    // Hardhat always runs the compile task when running scripts with its command
    // line interface.
    //
    // If this script is run directly using `node` you may want to call compile 
    // manually to make sure everything is compiled
    // await hre.run('compile');

    // We get the contract to deploy
    const PancakeFactory = await hre.ethers.getContractAt("Factory", "0x6725F303b657a9451d8BA641348b6761A6CC7a17");
    const MGTAddress = '0x3bB24f9cDCF739Ac0d6689d749450244D6605d92'
    const wBnbAddress = '0xae13d989daC2f0dEbFf460aC112a837C89BAa7cd'
    const createPair = await PancakeFactory.createPair(MGTAddress, wBnbAddress, { gasLimit: 250000 })
    console.log(createPair);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
    });