// We require the Hardhat Runtime Environment explicitly here. This is optional 
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const { ethers } = require("hardhat");
const hre = require("hardhat");

async function main() {
    // Hardhat always runs the compile task when running scripts with its command
    // line interface.
    //
    // If this script is run directly using `node` you may want to call compile 
    // manually to make sure everything is compiled
    // await hre.run('compile');

    // We get the contract to deploy
    const MyGToken = await hre.ethers.getContractFactory("MyGToken");
    // console.log(MyGToken);
    const myGToken = MyGToken.attach('0x3bB24f9cDCF739Ac0d6689d749450244D6605d92');
    const signers = await ethers.getSigners();
    console.log(signers[0].address)
    console.log((await myGToken.balanceOf(signers[0].address)).toString())
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
    });