// We require the Hardhat Runtime Environment explicitly here. This is optional 
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");

async function main() {
    // Hardhat always runs the compile task when running scripts with its command
    // line interface.
    //
    // If this script is run directly using `node` you may want to call compile 
    // manually to make sure everything is compiled
    // await hre.run('compile');

    // We get the contract to deploy
    // getContractAt
    const PancakeRouter = await hre.ethers.getContractAt("Router", "0xD99D1c33F9fC3444f8101754aBC46c52416550D1");
    const MGTAddress = '0x3bB24f9cDCF739Ac0d6689d749450244D6605d92'
    const wBnbAddress = '0xae13d989daC2f0dEbFf460aC112a837C89BAa7cd'
    const signers = await ethers.getSigners();
    const myGToken = (await hre.ethers.getContractFactory("MyGToken")).attach(MGTAddress);
    await myGToken.approve(PancakeRouter.address, 1000000);
    const WBNB = (await hre.ethers.getContractFactory("WBNB")).attach(wBnbAddress);
    await WBNB.approve(PancakeRouter.address, 10000);
    const addLiquidity = await PancakeRouter.addLiquidity(
        MGTAddress,
        wBnbAddress,
        10000,
        10000,
        10000,
        10000,
        signers[0].address,
        Math.floor(Date.now() / 1000) + 60 * 10, {
        gasLimit: 250000
    }
    )
    console.log(addLiquidity);
    // const myGToken = MyGToken.attach('0x3bB24f9cDCF739Ac0d6689d749450244D6605d92');
    // const signers = await ethers.getSigners();
    // console.log(signers[0].address)
    // console.log((await myGToken.balanceOf(signers[0].address)).toString())
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
    });