const { expect } = require("chai");

describe("MyGToken", function () {
  it("Should return the new greeting once it's changed", async function () {
    const MyGToken = await ethers.getContractFactory("MyGToken");
    const myGToken = await MyGToken.deploy(10000000000000);

    await myGToken.deployed();
    expect(await myGToken.greet()).to.equal("Hello, world!");

    await greeter.setGreeting("Hola, mundo!");
    expect(await greeter.greet()).to.equal("Hola, mundo!");
  });
});
